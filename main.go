package main

import (
	"fmt"

	go_say_hello "gitlab.com/rafli024/go-say-hello"
)

func main() {
	fmt.Println(go_say_hello.SayHello())
}